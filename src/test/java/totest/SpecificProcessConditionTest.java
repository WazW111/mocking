package totest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import touse.AccessRightsHelper;
import touse.AccessRightsRequest;
import touse.Contract;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(MockitoJUnitRunner.class)
@PrepareForTest(AccessRightsRequest.class)
//@RunWith(MockitoJUnitRunner.class)
public class SpecificProcessConditionTest {

    private SpecificProcessCondition processCondition;

    public static final String PROCESS = "NK";
    public static final String DATA = "Data";
    public static final String M1 = "M1";
    public static final String M2 = "M2";
    public static final String M3 = "M3";

    @Mock
    private Contract contract;

    private Contract spyContract;

    @Mock
    private AccessRightsRequest request;

    @Mock
    private AccessRightsHelper accessRightsHelper;

    @Before
    public void setUp() {
        // can't do it via annotation due to PowerMockRunner
        spyContract = spy(Contract.class);
        processCondition = new SpecificProcessCondition();
        processCondition.setAccessRightsHelper(accessRightsHelper);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(contract, request, accessRightsHelper, spyContract);
    }

    @Test(expected = RuntimeException.class)
    public void testIsAvailableProcessIsEmpty() {
        processCondition.isAvailable(null);
    }

    @Test(expected = RuntimeException.class)
    public void testIsAvailableContractIsNull() {
        processCondition.setProcess(PROCESS);
        processCondition.isAvailable(null);
    }

    @Test
    public void testIsAvailableContractNotAllowedForProcess() {
        processCondition.setProcess(PROCESS);
        when(contract.isAllowedFor(PROCESS)).thenReturn(false);

        boolean isAvailable = processCondition.isAvailable(contract);

        assertFalse(isAvailable);

        verify(contract).isAllowedFor(PROCESS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAvailableContractAllowedButEmptyRequest() {
        processCondition.setProcess(PROCESS);
        when(contract.isAllowedFor(PROCESS)).thenReturn(true);
        when(contract.getData()).thenReturn(DATA);
        PowerMockito.mockStatic(AccessRightsRequest.class);
        PowerMockito.when(AccessRightsRequest.createRequest(DATA)).thenReturn(null);

        try {
            processCondition.isAvailable(contract);
        } finally {
            verify(contract).isAllowedFor(PROCESS);
            verify(contract).getData();
        }
    }

    @Test
    public void testIsAvailableContractAllowedNoAccessRights() {
        processCondition.setProcess(PROCESS);
        when(contract.isAllowedFor(PROCESS)).thenReturn(true);
        when(contract.getData()).thenReturn(DATA);

        PowerMockito.mockStatic(AccessRightsRequest.class);
        PowerMockito.when(AccessRightsRequest.createRequest(DATA)).thenReturn(request);

        when(accessRightsHelper.hasAccessRights(request)).thenReturn(false);

        boolean isAvailable = processCondition.isAvailable(contract);

        assertFalse(isAvailable);

        verify(contract).isAllowedFor(PROCESS);
        verify(contract).getData();
        verify(accessRightsHelper).hasAccessRights(request);
    }

    @Test
    public void testIsAvailableContractAllowedWithAccessRightsButNoMsisdns() {
        processCondition.setProcess(PROCESS);
        when(contract.isAllowedFor(PROCESS)).thenReturn(true);
        when(contract.getData()).thenReturn(DATA);
        when(contract.getMsisdns()).thenReturn(Collections.EMPTY_LIST);
        PowerMockito.mockStatic(AccessRightsRequest.class);
        PowerMockito.when(AccessRightsRequest.createRequest(DATA)).thenReturn(request);
        when(accessRightsHelper.hasAccessRights(request)).thenReturn(true);

        boolean isAvailable = processCondition.isAvailable(contract);

        assertFalse(isAvailable);

        verify(contract).isAllowedFor(PROCESS);
        verify(contract).getData();
        verify(contract).getMsisdns();
        verify(accessRightsHelper).hasAccessRights(request);
    }

    @Test
    public void testIsAvailableContractAllowedWithAccessRightsButWrongMsisdn() {
        processCondition.setProcess(PROCESS);
        processCondition.setMsisdn(M3);

        when(contract.isAllowedFor(PROCESS)).thenReturn(true);
        when(contract.getData()).thenReturn(DATA);
        when(contract.getMsisdns()).thenReturn(Arrays.asList(M1, M2));

        PowerMockito.mockStatic(AccessRightsRequest.class);
        PowerMockito.when(AccessRightsRequest.createRequest(DATA)).thenReturn(request);

        when(accessRightsHelper.hasAccessRights(request)).thenReturn(true);

        boolean isAvailable = processCondition.isAvailable(contract);

        assertFalse(isAvailable);

        verify(contract).isAllowedFor(PROCESS);
        verify(contract).getData();
        verify(contract).getMsisdns();
        verify(accessRightsHelper).hasAccessRights(request);
    }


    @Test
    public void testIsAvailableAllConditionsFulfilled() {
        processCondition.setProcess(PROCESS);
        processCondition.setMsisdn(M2);

        when(contract.isAllowedFor(PROCESS)).thenReturn(true);
        when(contract.getData()).thenReturn(DATA);
        when(contract.getMsisdns()).thenReturn(Arrays.asList(M1, M2));

        PowerMockito.mockStatic(AccessRightsRequest.class);
        PowerMockito.when(AccessRightsRequest.createRequest(DATA)).thenReturn(request);

        when(accessRightsHelper.hasAccessRights(request)).thenReturn(true);

        boolean isAvailable = processCondition.isAvailable(contract);

        assertTrue(isAvailable);

        verify(contract).isAllowedFor(PROCESS);
        verify(contract).getData();
        verify(contract).getMsisdns();
        verify(accessRightsHelper).hasAccessRights(request);
    }

    @Test
    public void testCompareMockAndSpy1(){
        contract.setData("data");
        spyContract.setData("data");

        assertNull(contract.getData());
        assertEquals("data", spyContract.getData());
        verify(contract).getData();
        verify(contract).setData("data");
        verify(spyContract).getData();
        verify(spyContract).setData("data");

        contract.setMsisdns(Arrays.asList("1", "2"));
        spyContract.setMsisdns(Arrays.asList("1", "2"));
        assertEquals(0, contract.getMsisdns().size());
        assertEquals(2, spyContract.getMsisdns().size());

        when(contract.getMsisdns()).thenReturn(Arrays.asList("1", "2", "3"));


        verify(contract).setMsisdns(Arrays.asList("1", "2"));
        verify(spyContract).setMsisdns(Arrays.asList("1", "2"));
        verify(contract).getMsisdns();
        verify(spyContract).getMsisdns();
    }

    @Test
    public void testCompareMockAndSpy2(){
        contract.setMsisdns(Arrays.asList("1", "2"));
        spyContract.setMsisdns(Arrays.asList("1", "2"));
        assertEquals(0, contract.getMsisdns().size());
        assertEquals(2, spyContract.getMsisdns().size());

        when(contract.getMsisdns()).thenReturn(Arrays.asList("1", "2", "3"));
        when(spyContract.getMsisdns()).thenReturn(Arrays.asList("1"));
        assertEquals(3, contract.getMsisdns().size());
        assertEquals(1, spyContract.getMsisdns().size());


        verify(contract).setMsisdns(Arrays.asList("1", "2"));
        verify(spyContract).setMsisdns(Arrays.asList("1", "2"));
        verify(contract, times(2)).getMsisdns();
        verify(spyContract, times(2)).getMsisdns();
    }
}