package touse;

import java.util.List;

public class Contract {

    private String data;
    private List<String> allowedProcesses;
    private List<String> msisdns;

    private AccessRightsRequest request;

    public boolean isAllowedFor(String process){
        return !allowedProcesses.isEmpty() && allowedProcesses.contains(process);
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public List<String> getAllowedProcesses() {
        return allowedProcesses;
    }

    public void setAllowedProcesses(List<String> allowedProcesses) {
        this.allowedProcesses = allowedProcesses;
    }

    public List<String> getMsisdns() {
        return msisdns;
    }

    public void setMsisdns(List<String> msisdns) {
        this.msisdns = msisdns;
    }

    public AccessRightsRequest getRequest() {
        return request;
    }

    public void setRequest(AccessRightsRequest request) {
        this.request = request;
    }
}
