package totest;

import touse.AccessRightsHelper;
import touse.AccessRightsRequest;
import touse.Contract;
import touse.MyStringUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ProcessCondition {

    private String process;

    private AccessRightsHelper accessRightsHelper;

    private final static Logger LOGGER = Logger.getLogger("ProcessCondition");

    public boolean isAvailable(Contract contract) {
        if (!isAllowedForProcess(contract)) {
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.info("Contract is not allowed for process.");
            }
            return false;
        }
        if (!hasAccessRights(contract)) {
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.info("Contract doesn't have access rights.");
            }
            return false;
        }
        if (!checkSpecificConditions(contract)) {
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.info("Contract does not allow for specific process.");
            }
            return false;
        }

        return true;
    }

    protected abstract boolean checkSpecificConditions(Contract contract);

    /**
     * Checks if given contract has access rights for defined access right.
     *
     * @param contract
     * @return
     */
    private boolean hasAccessRights(Contract contract) {
        AccessRightsRequest request = AccessRightsRequest.createRequest(contract.getData());
//        AccessRightsRequest request = contract.getRequest();

        if (request == null) {
            throw new IllegalArgumentException("Wrong data!");
        }

        return accessRightsHelper.hasAccessRights(request);

    }


    private boolean isAllowedForProcess(Contract contract) {
        if (MyStringUtils.isEmpty(process) || contract == null) {
            throw new RuntimeException("Wrong data!");
        }

        return contract.isAllowedFor(process);
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public AccessRightsHelper getAccessRightsHelper() {
        return accessRightsHelper;
    }

    public void setAccessRightsHelper(AccessRightsHelper accessRightsHelper) {
        this.accessRightsHelper = accessRightsHelper;
    }
}
