package totest;

import touse.Contract;

import java.util.List;

public class SpecificProcessCondition extends ProcessCondition {

    private String msisdn;

    @Override
    protected boolean checkSpecificConditions(Contract contract) {
        List<String> msisdns = contract.getMsisdns();

        if(msisdns != null && !msisdns.isEmpty()){
            return msisdns.contains(msisdn);
        }

        return false;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}
